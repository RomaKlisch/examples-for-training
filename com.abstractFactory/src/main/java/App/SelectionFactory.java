package App;


public class SelectionFactory {
    public static AbstractPattern getFactory(String choice){
        if (choice.equals("ConnectBD")){
            System.out.println("ConnectBD");
            return new ConnectBDFactory();
        }
        if (choice.equals("RequestBD")){
            System.out.println("RequestBD");
            return new RequestBDFactory();
        }
        return null;
    }
}
