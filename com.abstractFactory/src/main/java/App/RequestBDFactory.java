package App;


public class RequestBDFactory extends AbstractPattern {

    @Override
    ConnectBD getBD(String nameBD) {
        return null;
    }

    @Override
    RequestBD getUser(String user) {
        if (user.equals("Student")){
            return new Student();
        }
        if (user.equals("Teacher")){
            return new Teacher();
        }
        return null;
    }
}
