package App;


public class ConnectBDFactory extends AbstractPattern {
    @Override
    ConnectBD getBD(String nameBD) {
        if (nameBD.equals("H2")){
            return new BD_H2();
        }
        if (nameBD.equals("MySQl")){
            return new BD_MySQL();
        }
        return null;
    }

    @Override
    Student getUser(String user) {
        return null;
    }
}
