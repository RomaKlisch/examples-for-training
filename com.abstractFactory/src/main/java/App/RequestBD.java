package App;


import java.util.List;
import java.util.Objects;

public interface RequestBD {

    void initBD(Objects... params);
    void deleteUser(Object... params);
}
