package App;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AbstractPattern abstractPattern=SelectionFactory.getFactory("ConnectBD");
        abstractPattern.getBD("H2");
        abstractPattern.getUser("Student");

        AbstractPattern abstractPattern1=SelectionFactory.getFactory("RequestBD");
        abstractPattern1.getUser("Teacher");

    }
}
