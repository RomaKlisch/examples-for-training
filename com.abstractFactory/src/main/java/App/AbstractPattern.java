package App;


abstract class AbstractPattern {
    abstract ConnectBD getBD(String nameBD);
    abstract RequestBD getUser(String user);
}
