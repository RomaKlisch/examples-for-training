package App;


import java.util.*;

public class App
{
    public static void main( String[] args )
    {
        Person person=null;
        Person person1=new Person(null,12,"c");
        Person person2=new Person("a",12,"a");
        Person person3=new Person("a",12,"a");
        Person person4=new Person("z",12,"z");
        Person person5=new Person("b",12,"b");



//        Map<Integer,Person> listPerson=new HashMap<>();
//        listPerson.put(null,null);
//        listPerson.put(null,person);
//        listPerson.put(1,person1);
//        listPerson.put(2,person2);
//        listPerson.put(4,person5);
//
//        for (Map.Entry entry:listPerson.entrySet()){
//            System.out.println(entry.getKey()+"    "+entry.getValue());
//        }

        Map<Integer,Person> listPersonTree=new TreeMap<>();
        listPersonTree.put(0,null);
        listPersonTree.put(3,person);
        listPersonTree.put(1,person1);
        listPersonTree.put(2,person2);
        listPersonTree.put(4,person5);

        for (Map.Entry entry:listPersonTree.entrySet()){
            System.out.println(entry.getKey()+"    "+entry.getValue());
        }




//        Set<Person> listPersonSet=new HashSet<>();
//        listPersonSet.add(person);
//        listPersonSet.add(person1);
//        listPersonSet.add(person3);
//        listPersonSet.add(person3);
//        listPersonSet.add(person4);
//        listPersonSet.add(person5);
//
//        Iterator iterator=listPersonSet.iterator();
//        while (iterator.hasNext()){
//            System.out.println(iterator.next());
//        }


        Set<Person> listPersonTreeSet=new TreeSet<>();
//        listPersonTreeSet.add(person);
//        listPersonTreeSet.add(person1);
        listPersonTreeSet.add(person3);
        listPersonTreeSet.add(person3);
        listPersonTreeSet.add(person4);
        listPersonTreeSet.add(person5);

        Iterator iterator1=listPersonTreeSet.iterator();
        while (iterator1.hasNext()){
            System.out.println(iterator1.next());
        }






    }
}
