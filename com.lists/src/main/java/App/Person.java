package App;


import java.util.Iterator;

public class Person implements Comparable<Person> {
    private String name;
    private int age;
    private String secondName;

    public Person(String name, int age, String secondName) {
        this.name = name;
        this.age = age;
        this.secondName = secondName;
    }

    public String getName() {
        return name;
}

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        return !(secondName != null ? !secondName.equals(person.secondName) : person.secondName != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        return result;
    }

    public int compareTo(Person obj) {
        Person person=obj;
        int rezult=person.age-age;
        if (rezult!=0){
            return rezult;
        }
        rezult=person.name.compareTo(name);
        if (rezult!=0){
            return rezult;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", secondName='" + secondName + '\'' +
                '}';
    }

}
//    public int compareTo(Object obj) {
//        Comp entry = (Comp) obj;
//
//        int result = entry.str.compareTo(str); // значения меняются местами
//        if(result != 0) {
//            return result;
//        }
//
//        result = entry.number - number; // значения меняются местами
//        if(result != 0) {
//            return (int) result / Math.abs( result );
//        }
//        return 0;
//    }