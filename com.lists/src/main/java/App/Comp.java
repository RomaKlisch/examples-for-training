package App;


public class Comp implements Comparable<Comp> {
    String str;
    int number;

    Comp(String str, int number) {
        this.str = str;
        this.number = number;
    }

    @Override
    public int compareTo(Comp obj) {
        Comp entry = obj;

        int result = entry.str.compareTo(str); // значения меняются местами
        if (result != 0) {
            return result;
        }

        result = entry.number - number; // значения меняются местами
        if (result != 0) {
            return (int) result / Math.abs(result);
        }
        return 0;
    }
}

