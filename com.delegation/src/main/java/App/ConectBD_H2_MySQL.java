package App;


public class ConectBD_H2_MySQL implements ConectBD {
    ConectBD conectBD;

    public void toH2(){
        conectBD=new BD_H2();
    }
    public void toMySQL(){
        conectBD=new BD_MySQL();
    }

    @Override
    public void conectBD() {
        conectBD.conectBD();
    }
}
