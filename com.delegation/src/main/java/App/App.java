package App;


public class App 
{
    public static void main( String[] args )
    {
       ConectBD_H2_MySQL conectBD_h2_mySQL=new ConectBD_H2_MySQL();

        conectBD_h2_mySQL.toH2();
        conectBD_h2_mySQL.conectBD();

        conectBD_h2_mySQL.toMySQL();
        conectBD_h2_mySQL.conectBD();

    }
}
