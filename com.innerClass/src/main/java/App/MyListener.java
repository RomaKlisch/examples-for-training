package App;


import java.util.ArrayList;
import java.util.List;

public class MyListener {

    private void reaction (String sms){
        System.out.println("Do somethins");
    }

    class ListenerMy implements Listener{
        private MyListener myListener;

        public ListenerMy(MyListener myListener) {
            this.myListener = myListener;
        }

        public ListenerMy() {

        }

        @Override
        public void onEvent(String sms) {
           myListener.reaction(sms);
            MyListener.this.reaction(sms); //тоже самое, не будет работать в статик классах
        }
    }

    public void initSms(Event event){
        event.addListener(new ListenerMy());
    }
}
interface Listener{
    void onEvent(String sms);

}
class Event implements Runnable{

    private final List<Listener> listeners=new ArrayList();

    void addListener(Listener listener){
        listeners.add(listener);
    }
    @Override
    public void run() {

        for (Listener l:listeners){
            l.onEvent("Hello");
        }
    }
}