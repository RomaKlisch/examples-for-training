package App;


public class Dog implements SayZdraste {

    private String field;
    static private String fieldStatic;


    public static int temp() {
        return 5;
    }

    private String say() {
        return "Say";
    }


    private class Mops {
        private String fieldMops;
        //static private String fieldStatic;
        public Mops(String sayMops) {
            this.fieldMops = sayMops;
            fieldStatic="MopsGavStatic";
            field="fieldSuperClass";
        }

        private void sayMops() {
            System.out.println("MopsGav");
        }

    }

    static class Buldog {
        private String sayBuldog;
        static private String sayBuldogStatic;

        public Buldog(String sayBuldog) {
            this.sayBuldog = sayBuldog;
            fieldStatic = "BuldogGavStatic";
        }

        private void sayBuldog() {
            System.out.println("BuldogGav");
        }

        private static void sayBuldogStatic() {
            System.out.println("BuldogGav");
        }

    }

        class StreetDog {
            private String sayStreetDog;
            //static private String fieldStatic;

            public StreetDog(String sayStreetDog) {
                this.sayStreetDog = sayStreetDog;
                field="fieldSuperClass";
                fieldStatic="StreetGogGavStatic";
            }

            private void sayStreetDog() {
                System.out.println("StreetDogGav");
            }
        }


    @Override
      public void sayGoodMorning() {

    }

    @Override
    public String[] sayHello(int temp) {
        return new String[0];
    }

    @Override
    public void sayHi(String hi) {

    }
}



abstract class Shpic implements SayZdraste{

    @Override
    public void sayGoodMorning() {
        System.out.println("GoodMorning");
    }

    @Override
    public String[] sayHello(int temp) {
        System.out.println("Hello");
        return new String[0];
    }

    @Override
    public void sayHi(String hi) {
        System.out.println(hi);
    }
}

interface SayDog extends SayZdraste{

    @Override
    void sayGoodMorning();

    @Override
    String[] sayHello(int temp);

    @Override
    void sayHi(String hi);
}

