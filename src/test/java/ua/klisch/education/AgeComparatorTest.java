package ua.klisch.education;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ua.klisch.education.model.AgeComparator;
import ua.klisch.education.model.Person;

public class AgeComparatorTest {

    public static final int SECOND_VALUE_IS_GREATER = -1;

    public static final int FIRST_VALUE_IS_GREATER = 1;

    public static final int VALUES_ARE_EQUAL = 0;

    private AgeComparator comparator = new AgeComparator();

    private Person person1;

    private Person person2;

    @Before
    public void setUp() {
        person1 = new Person(10);
        person2 = new Person(20);
    }

    @Test
    public void shouldCompareTwoPersonsByAge() {
        assertEquals(SECOND_VALUE_IS_GREATER, comparator.compare(person1, person2));
    }

    @Test
    public void shouldCompareTwoPersonsByAgeInReverseOrder() {
        assertEquals(FIRST_VALUE_IS_GREATER, comparator.compare(person2, person1));
    }

    @Test
    public void shouldDetectEqualPersonsByAge() {
        assertEquals(VALUES_ARE_EQUAL, comparator.compare(new Person(15), new Person(15)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnNulls() {
        comparator.compare(null, null);
    }

}
