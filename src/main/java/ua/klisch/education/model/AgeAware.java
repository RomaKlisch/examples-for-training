package ua.klisch.education.model;

public interface AgeAware {

    int getAge();

}
