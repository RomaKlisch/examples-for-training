package ua.klisch.education.model;

import java.util.Comparator;

public class FirstNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person arg0, Person arg1) {
        if (arg0 == null || arg1 == null) {
            throw new IllegalArgumentException("Not null params were expected!");
        }
        return compare(arg0.getFirstName(), arg1.getFirstName());
    }

    private int compare(String arg0, String arg1) {
        if (arg0 == null) {
            return arg1 == null ? 0 : -1;
        } else {
            return arg0.compareTo(arg1);
        }
    }

}
