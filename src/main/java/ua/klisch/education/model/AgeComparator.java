package ua.klisch.education.model;

import java.util.Comparator;

public class AgeComparator implements Comparator<AgeAware> {

    @Override
    public int compare(AgeAware arg0, AgeAware arg1) {
        if (arg0 == null || arg1 == null) {
            throw new IllegalArgumentException("Not null params were expected!");
        } else if (arg0.getAge() == arg1.getAge()) {
            return 0;
        } else {
            return arg0.getAge() > arg1.getAge() ? 1 : -1;
        }
    }

}
