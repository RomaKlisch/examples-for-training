package App;


import java.util.Arrays;

public class Say implements SayZdraste {

    @Override
    public void sayGoodMorning() {
        System.out.println("Hi");
    }

    @Override
    public String[] sayHello(int temp) {
        String[] mass=new String[temp];
        for (int i = 0; i <temp ; i++) {
            mass[i]="Hello";
        }
        System.out.println(Arrays.toString(mass));
        return mass;
    }

    @Override
    public void sayHi(String hi) {
        System.out.println(hi);
    }

}
