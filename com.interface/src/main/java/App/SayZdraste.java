package App;


 public interface SayZdraste extends SayHi, SayGoodMorning, SayHello {

    @Override
    void sayGoodMorning();

     @Override
     String[] sayHello(int temp);

     @Override
     void sayHi(String hi);
 }
