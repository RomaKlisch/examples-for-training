package App;


import java.util.Random;

public class ConstructorApp2 extends ConstructorApp1 {

    private ConstructorApp1 constructorApp1;
    private int temp;
    static int grup;

    public ConstructorApp2(int age, String name, int temp) {
        super(age, name);
        this.temp = temp;
        System.out.println(System.currentTimeMillis()+"  App2");
    }

    static {
        Random random=new Random();
        grup=random.nextInt(100);
        System.out.println(System.currentTimeMillis()+"  "+grup+"\n");
    }

    public void printApp2(){
        System.out.println("printApp2");
    }

    @Override
    public String toString() {
        return "ConstructorApp2{" +
                "constructorApp1=" + constructorApp1 +
                ", temp=" + temp +
                '}';
    }
}
