package App;



public class ConstructorApp1  {
    private int age;
    private  String name;
    static String secondName;

    public ConstructorApp1(int age, String name) {
        this.age = age;
        this.name = name;
        System.out.println(System.currentTimeMillis()+"  App1");
    }
    static {
        secondName="Const";
        System.out.println(System.currentTimeMillis()+"  "+secondName+"\n");
    }

    public ConstructorApp1() {
    }

    public void printApp1(){
        System.out.println("printApp1");
    }

    @Override
    public String toString() {
        return "ConstructorApp1{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
