package App;


public class ConstructorStaticClass {
    private static int first;
    private int second;

    public ConstructorStaticClass(int second) {
        this.second = second;
        System.out.println(System.currentTimeMillis()+"  Static");
    }
    public ConstructorStaticClass() {    }


    public static void  seeFirst(){
        System.out.println("  static parents");
    }
}
